// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';

import '../const.dart';

class MyButton extends StatelessWidget {
  final String child;
  final VoidCallback onTap;

  MyButton({
    Key? key,
    required this.child,
    required this.onTap,
  }) : super(key: key);
  var buttonColor = Colors.pink.shade400;

  @override
  Widget build(BuildContext context) {
    if (child == 'DEL') {
      buttonColor = Colors.red.shade500;
    } else if (child == '=') {
      buttonColor = Colors.green;
    }
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            color: buttonColor,
            borderRadius: BorderRadius.circular(8),
          ),
          child: Center(
            child: Text(
              child,
              style: whiteTextStyle,
            ),
          ),
        ),
      ),
    );
  }
}

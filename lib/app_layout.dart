abstract class AppLayout {
  static const double widthFirstBreakpoint = 450.0;
  static const double maximumContentWidthDetail = 800.0;
}

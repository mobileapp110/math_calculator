import 'package:flutter/material.dart';

var whiteTextStyle = const TextStyle(
    fontWeight: FontWeight.bold, fontSize: 30, color: Colors.white);
var timeTextStyle = const TextStyle(
    fontWeight: FontWeight.bold, fontSize: 25, color: Colors.white);

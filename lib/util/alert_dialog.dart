// ignore_for_file: prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import '../const.dart';

class AlertMessageDialog extends StatelessWidget {
  final String message;
  final VoidCallback onTap;
  final icon;

  const AlertMessageDialog({
    Key? key,
    required this.message,
    required this.onTap,
    required this.icon,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.pink.shade300,
      content: SizedBox(
        height: 200,
        child:
            Column(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
          Text(
            message,
            style: whiteTextStyle,
          ),
          GestureDetector(
            onTap: onTap,
            child: Container(
              padding: const EdgeInsets.all(4.0),
              decoration: BoxDecoration(
                  color: Colors.pink.shade200,
                  borderRadius: BorderRadius.circular(8)),
              child: Icon(
                icon,
                color: Colors.white,
                size: 50,
              ),
            ),
          )
        ]),
      ),
    );
  }
}

import 'package:flutter/widgets.dart';

class NumpadProvider with ChangeNotifier {
  List<String> numPads = [
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
    'DEL',
    '0',
    '=',
  ];
  //ดึงข้อมูล
  int get count => numPads.length;
  List<String> get getNumpad => numPads;
}

class Numpad {
  String numPad;

  Numpad({required this.numPad});
}

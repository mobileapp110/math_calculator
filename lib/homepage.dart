import 'package:flutter/material.dart';
import 'package:math_calculator/main_menu.dart';

import 'app_layout.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final smallerDeviceLanscape = height < AppLayout.widthFirstBreakpoint;
    final isLanscape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
          color: Colors.black12,
          child: Center(
            child: ListView(
              shrinkWrap: true,
              children: [
                Container(
                  margin: EdgeInsets.all(
                    (smallerDeviceLanscape && isLanscape) ? 15 : 30,
                  ),
                  width: (smallerDeviceLanscape && isLanscape) ? 200 : 400,
                  height: (smallerDeviceLanscape && isLanscape) ? 200 : 400,
                  child: Center(
                      //Change Text to ImageNetwork
                      //1.
                      child: Image.network(
                          'https://media.discordapp.net/attachments/900989019373072439/1083340507918372885/logo.png')),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 1),
                  child: Center(
                      child: InkWell(
                    borderRadius: BorderRadius.circular(45),
                    onTap: () async {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return const MainMenu();
                      }));
                    }, // Handle your callback
                    child: Ink(
                      width: (smallerDeviceLanscape && isLanscape) ? 100 : 200,
                      height: (smallerDeviceLanscape && isLanscape) ? 35 : 70,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(45),
                        color: Colors.pink.shade400,
                      ),
                      child: Container(
                        alignment: Alignment.center,
                        child: Text(
                          "START",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: (smallerDeviceLanscape && isLanscape)
                                ? 35 / 2
                                : 35,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ),
                    ),
                  )),
                )
              ],
            ),
          )),
    );
  }
}

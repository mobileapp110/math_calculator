import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:math_calculator/homepage.dart';
import 'package:math_calculator/util/alert_dialog.dart';
import 'package:math_calculator/util/my_button.dart';
import 'package:math_calculator/util/numpad_provider.dart';
import 'app_layout.dart';
import 'const.dart';

class MainMenu extends StatefulWidget {
  const MainMenu({Key? key}) : super(key: key);
  @override
  State<MainMenu> createState() => _MainMenuState();
}

class _MainMenuState extends State<MainMenu> {
  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    final smallerDeviceLanscape = height < AppLayout.widthFirstBreakpoint;
    final isLanscape =
        MediaQuery.of(context).orientation == Orientation.landscape;
    final checkDevice = smallerDeviceLanscape && isLanscape;
    return Scaffold(
        backgroundColor: Colors.black,
        body: checkDevice ? buildBodyLandScape(width) : buildBody(width));
  }

  int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 31;
  // List<String> numPad = [
  //   '1',
  //   '2',
  //   '3',
  //   '4',
  //   '5',
  //   '6',
  //   '7',
  //   '8',
  //   '9',
  //   'DEL',
  //   '0',
  //   '=',
  // ];
  int count = 0;
  int numberA = Random().nextInt(10);
  int numberB = Random().nextInt(10);
  String userAnswer = '';
  var btn = true;
  //user tapped a button
  void buttonTapped(String button) {
    if (!btn) {
      return goBackToQuestion();
    }
    setState(() {
      if (button == '=') {
        // calculate if user is correct or incorrect
        checkResult();
      } else if (button == 'DEL') {
        // delete the last number
        userAnswer = '';
      } else if (userAnswer.length < 3) {
        // maximum of 3 numbeers can be inputted
        userAnswer += button;
      }
    });
  }

// check if user is correct or not
  void checkResult() {
    if (numberA + numberB == int.parse(userAnswer)) {
      goToNextQuestion();
    } else {
      goBackToQuestion();
    }
  }

  countdown(width) {
    return CountdownTimer(
      endTime: endTime,
      widgetBuilder: (_, time) {
        if (time == null) {
          btn = false;
          return Container(
            height: 100,
            width: width,
            color: Colors.pink.shade400,
            padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
            child: Center(
              child: TextButton(
                  onPressed: goBackToQuestion,
                  child: Text("Time's Up", style: timeTextStyle)),
            ),
          );
        }
        return Container(
          height: 100,
          width: width,
          color: Colors.pink.shade400,
          padding: const EdgeInsets.fromLTRB(0, 15, 0, 0),
          child: Center(
            child: Text(
              '${time.sec}',
              style: timeTextStyle,
            ),
          ),
        );
      },
    );
  }

  var randomNumber = Random();

  void goToNextQuestion() {
    //close dialog
    // Navigator.of(context).pop();
    setState(() {
      userAnswer = '';
    });
    count++;
    numberA = randomNumber.nextInt(10);
    numberB = randomNumber.nextInt(10);
  }

  void goBackToQuestion() {
    if (!btn) {
      showDialog(
        context: context,
        builder: (context) {
          return AlertMessageDialog(
            message: 'Score: $count\nPlay again.',
            onTap: restartGame,
            icon: Icons.play_circle_outlined,
          );
        },
      );
    }
    setState(() {
      userAnswer = '';
    });
  }

  void restartGame() {
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) {
          return const HomePage();
        },
      ),
    );
    // Navigator.of(context).pop();
    setState(() {
      userAnswer = '';
    });
    count = 0;
    btn = true;
    numberA = randomNumber.nextInt(10);
    numberB = randomNumber.nextInt(10);
  }

  Widget buildBody(width) {
    return Column(
      children: [
        //time
        countdown(width),
        Expanded(
          child: Container(
            padding: const EdgeInsets.fromLTRB(0, 70, 0, 0),
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //question
                  Text(
                    '$numberA + $numberB = ',
                    style: whiteTextStyle,
                  ),
                  //ans
                  Container(
                    height: 50,
                    width: 100,
                    decoration: BoxDecoration(
                        color: Colors.pink.shade400,
                        borderRadius: BorderRadius.circular(4)),
                    child:
                        Center(child: Text(userAnswer, style: whiteTextStyle)),
                  ),
                ],
              ),
            ),
          ),
        ),
        //btnNumber
        Expanded(
          flex: 4,
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 30, 20, 0),
            child: GridView.builder(
              itemCount: NumpadProvider().count,
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                childAspectRatio: 1,
              ),
              itemBuilder: (context, int index) {
                return MyButton(
                  child: NumpadProvider().numPads[index],
                  onTap: () => buttonTapped(NumpadProvider().numPads[index]),
                );
              },
            ),
          ),
        ),
      ],
    );
  }

  Widget buildBodyLandScape(width) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Container(
      color: Colors.black,
      child: Column(
        children: [
          Row(
            children: [
              //time
              SizedBox(
                width: width,
                height: 60,
                child: countdown(width),
              ),
            ],
          ),
          Row(
            children: [
              Container(
                width: width / 2,
                height: height - 100,
                padding: const EdgeInsets.all(15),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      //question
                      Text(
                        '$numberA + $numberB = ',
                        style: whiteTextStyle,
                      ),
                      //ans
                      Container(
                        height: 50,
                        width: 100,
                        decoration: BoxDecoration(
                            color: Colors.pink.shade400,
                            borderRadius: BorderRadius.circular(4)),
                        child: Center(
                            child: Text(userAnswer, style: whiteTextStyle)),
                      ),
                    ],
                  ),
                ),
              ),
              //btnNumber
              Column(
                children: [
                  Container(
                    width: width / 2,
                    height: height - 70,
                    padding: const EdgeInsets.fromLTRB(0, 5, 0, 0),
                    child: GridView.builder(
                      itemCount: NumpadProvider().count,
                      physics: const NeverScrollableScrollPhysics(),
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 3,
                        crossAxisSpacing: 1,
                        mainAxisSpacing: 1,
                        childAspectRatio: 1.9,
                      ),
                      itemBuilder: (context, index) {
                        return MyButton(
                            child: NumpadProvider().numPads[index],
                            onTap: () =>
                                buttonTapped(NumpadProvider().numPads[index]));
                      },
                    ),
                  ),
                ],
              ),
            ],
          )
        ],
      ),
    );
  }
}

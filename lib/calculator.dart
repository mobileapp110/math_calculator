import 'package:flutter/material.dart';
import 'app_layout.dart';
import 'dart:math';

class MainView extends StatelessWidget {
  const MainView({super.key});

  randomNum() {
    // Generate a random integer between 0 and 9
    return Random().nextInt(10);
  }

  @override
  Widget build(BuildContext context) {
    final width = MediaQuery.of(context).size.width;
    final smallerDevice = width < AppLayout.widthFirstBreakpoint;
    const myDuration = Duration(seconds: 30);
    String strDigits(int n) => n.toString().padLeft(2, '0');
    final seconds = strDigits(myDuration.inSeconds.remainder(60));

    return Scaffold(
      body: smallerDevice
          ? Column(
              children: [
                const Padding(
                  padding: EdgeInsets.fromLTRB(23, 20, 15, 23),
                ),
                Text(
                  seconds,
                  style: const TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                      fontSize: 50),
                ),
                // const Padding(
                //   padding: EdgeInsets.fromLTRB(23, 5, 15, 0),
                //   child: Text(
                //     "24",
                //     style: TextStyle(color: Colors.black, fontSize: 40),
                //   ),
                // ),
                Row(
                  children: [
                    Padding(
                        padding: const EdgeInsets.fromLTRB(28, 30, 5, 0),
                        child: btnNumber(randomNum())),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(10, 30, 5, 0),
                        child: btnNumber(randomNum())),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(10, 30, 5, 0),
                        child: btnNumber(randomNum())),
                    Padding(
                        padding: const EdgeInsets.fromLTRB(10, 30, 5, 0),
                        child: btnNumber(randomNum())),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(23, 20, 15, 20),
                  child: btnOperatorNM(),
                ),

                const Padding(
                  padding: EdgeInsets.fromLTRB(23, 5, 15, 0),
                  child: Text(
                    "res",
                    style: TextStyle(color: Colors.black, fontSize: 40),
                  ),
                ),
              ],
            )
          : Padding(
              padding: const EdgeInsets.fromLTRB(73, 120, 73, 20),
              child: btnOperatorLS(),
            ),
    );
  }
}

Widget btnNumber(number) {
  return Container(
    width: 70,
    height: 70,
    color: const Color.fromARGB(255, 90, 255, 137),
    child: TextButton(
      onPressed: () {},
      child: Text(
        number.toString(),
        style: const TextStyle(color: Colors.black, fontSize: 25),
      ),
    ),
  );
}

Widget btnOperatorNM() {
  return Column(
    children: [
      Row(
        children: [
          Padding(padding: const EdgeInsets.all(10.0), child: btnPlus()),
          Padding(padding: const EdgeInsets.all(10.0), child: btnMinus())
        ],
      ),
      Row(
        children: [
          Padding(padding: const EdgeInsets.all(10.0), child: btnMultiply()),
          Padding(padding: const EdgeInsets.all(10.0), child: btnDivide())
        ],
      ),
    ],
  );
}

Widget btnOperatorLS() {
  return Row(
    children: [
      Column(
        children: [
          Padding(padding: const EdgeInsets.all(10.0), child: btnPlus()),
        ],
      ),
      Column(
        children: [
          Padding(padding: const EdgeInsets.all(10.0), child: btnMinus())
        ],
      ),
      Column(
        children: [
          Padding(padding: const EdgeInsets.all(10.0), child: btnMultiply()),
        ],
      ),
      Column(
        children: [
          Padding(padding: const EdgeInsets.all(10.0), child: btnDivide())
        ],
      )
    ],
  );
}

Widget btnPlus() {
  return Container(
    width: 150,
    height: 150,
    color: const Color.fromARGB(255, 90, 255, 137),
    child: IconButton(
      onPressed: () {},
      icon: const Icon(Icons.add),
    ),
  );
}

Widget btnMinus() {
  return Container(
    width: 150,
    height: 150,
    color: const Color.fromARGB(255, 90, 255, 137),
    child: IconButton(
      onPressed: () {},
      icon: const Icon(Icons.remove),
    ),
  );
}

Widget btnMultiply() {
  return Container(
    width: 150,
    height: 150,
    color: const Color.fromARGB(255, 90, 255, 137),
    child: TextButton(
      onPressed: () {},
      child: const Text(
        "×",
        style: TextStyle(color: Colors.black, fontSize: 25),
      ),
    ),
  );
}

Widget btnDivide() {
  return Container(
    padding: const EdgeInsets.all(5.0),
    width: 150,
    height: 150,
    color: const Color.fromARGB(255, 90, 255, 137),
    child: TextButton(
      onPressed: () {},
      child: const Text(
        "÷",
        style: TextStyle(color: Colors.black, fontSize: 25),
      ),
    ),
  );
}
